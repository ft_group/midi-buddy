export default class Gifster {
  img;
  duration = false;
  element;
  constructor(parent, path) {
    this.img = path;
    httpGetAsync(path, (res) => {
      this.duration = JSON.parse(res).duration;
    });
    this.element = document.createElement('img');
    this.element.classList.add('gif-container');
    this.element.style.display = 'none';
    parent.appendChild(this.element);
  }
}

function httpGetAsync(theUrl, callback) {
  const encodedPath = encodeURIComponent(theUrl);
  const url = `http://localhost:3003/gif-length/${encodedPath}`;
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function () {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
      callback(xmlHttp.responseText);
  };
  xmlHttp.open('GET', url, true); // true for asynchronous
  xmlHttp.send(null);
}
