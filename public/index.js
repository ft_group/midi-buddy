import * as utility from './midibuddy/utility.js';
import * as inputDeviceController from './midibuddy/inputDeviceController.js';
import * as publishSubscribe from './midibuddy/publishSubscribe.js';
import * as debug from './midibuddy/debug.js';
import NoteQueue from './midibuddy/notequeue.js';
import SongStorage from './midibuddy/songStorage.js';
import settings from './midibuddy/settings.js';
import PlayerPolySynth from './midibuddy/PlayerPolySynth.js';
import * as midiToNoteName from './midibuddy/midiToNoteName.js';
import Gifster from './gifster/index.js';

let gifControls;

const gifUrls = [];

for (let x = 0; x < 12; x++) {
  fetch(
    'https://api.giphy.com/v1/stickers/random?api_key=VYM8kH14nkTSIPKbqpVKoBcBx3Xf8YNi&tag=jesus&rating=r'
  )
    .then((res) => {
      return res.json();
    })
    .then((res) => {
      gifUrls.push(res.data.images.original.url.split('?')[0]);
      console.log(gifUrls);
      if (gifUrls.length === 12) {
        gifControls = gifUrls.map((path) => {
          return new Gifster(gifsterContainer, path);
        });
      }
    });
}

const gifsterContainer = document.getElementById('gifster-container');

let clearGIFTimers = {};

inputDeviceController.initializeDeviceList(
  document.getElementById('midi-list')
);

const noteQueue = new NoteQueue({
  id: 'notes',
  buffer: [],
});
const registeringQueue = new NoteQueue({
  id: 'registering',
  buffer: {},
});

const Song = new SongStorage();

const Player = new PlayerPolySynth();

publishSubscribe.on('midikeydown', (data) => {
  registeringQueue.addToQueue(
    { start: utility.now(), velocity: data.velocity },
    data.key
  );
});

publishSubscribe.on('midikeyup', (data) => {
  const noteData = registeringQueue.getItemAtIndex(data.key);
  const duration = utility.now() - noteData.start;
  registeringQueue.clearItemAtIndex(data.key);
  noteQueue.addToQueue(
    Object.assign(data, {
      start: noteData.start,
      duration: duration,
      velocity: noteData.velocity,
    })
  );
});

publishSubscribe.on('notequeued', (data) => {
  if (data.source === 'registering') {
    const note = data.data;
    Song.startNote(note.key, note.start, note.velocity);
  } else if (data.source === 'notes') {
    const note = data.data;
    Song.endNote(note.key, note.start, note.start + note.duration);
  }
});

document.addEventListener('keydown', (e) => {
  if (Tone.Transport.state === 'started') Tone.Transport.stop();
  if (e.key === ' ') {
    const synth = new Tone.PolySynth().toDestination();

    let melody = Song.getMelody(Song.getSong());
    const startTime = melody[0].start;
    melody = melody.map((n) => {
      n.start -= startTime;
      n.end -= startTime;
      n.time = n.start / 1000;
      n.duration = (n.end - n.start) / 1000;
      return n;
    });

    console.log(melody);

    const part = new Tone.Part((time, data) => {
      // the value is an object which contains both the note and the velocity
      synth.triggerAttackRelease(
        midiToNoteName.getNote(data.key),
        data.duration,
        time,
        utility.normalizeVelocity(data.velocity)
      );
    }, melody).start(0);
    Tone.Transport.start();
  }
});

async function playerNote(player) {
  const buffer = noteQueue.buffer;
  if (buffer.length === 0) {
    setTimeout(async () => {
      playerNote(Player);
    }, settings.oneBPM);
    return;
  }
}

const visualizer = document.getElementById('visualizer');
const colorArray = [];

fillColorArray(colorArray, 76);

console.log(colorArray);

function fillColorArray(arr, count) {
  let runs = 0;
  while (runs < count) {
    arr.push(getRandomHexColor());
    runs++;
  }
}

function getRandomHexColor() {
  const alphabet = '0123456789ABCDEF';
  let set = '#';
  while (set.length < 7) {
    set += alphabet[Math.floor(Math.random() * (alphabet.length - 1))];
  }

  return set;
}

// publishSubscribe.on('midikeydown', (data) => {
//   const index = data.key - 28;
//   const body = document.getElementsByTagName('body')[0];
//   body.style.background = colorArray[index];
//   body.classList.add('body-rumble');

//   // const emojiboy = document.createElement('div');
//   // emojiboy.classList.add('emoji');
//   // emojiboy.innerHTML = emoji[index];
//   // visualizer.appendChild(emojiboy);
//   // const location = [0, 0];
//   // const velocity = [getIntegerInRange(-20, 20), getIntegerInRange(-20, 20)];

//   // const size = getIntegerInRange(50, 176, data.velocity);
//   // emojiboy.style.fontSize = `${size}px`;

//   // const animator = setInterval(() => {
//   //   moveEmoji(emojiboy, location, velocity);
//   // }, 50);

//   const bodyMovin = document.createElement('img');
//   bodyMovin.classList.add('image-holder');
//   visualizer.appendChild(bodyMovin);
//   const bodyLocation = [0, 0];
//   const bodyVelocity = [getIntegerInRange(-20, 20), getIntegerInRange(-20, 20)];

//   const size = getIntegerInRange(1, 127, data.velocity);

//   const sizes = [203 * (size / 100), 335 * (size / 100)];
//   bodyMovin.style.width = sizes[0] + 'px';
//   bodyMovin.style.height = sizes[1] + 'px';

//   const bodyAnimator = setInterval(() => {
//     moveEmoji(bodyMovin, bodyLocation, bodyVelocity);
//   }, 50);

//   setTimeout(() => {
//     // clearInterval(animator);
//     clearInterval(bodyAnimator);
//     // emojiboy.remove();
//     bodyMovin.remove();
//   }, 2000);
// });

// publishSubscribe.on('midikeydown', (data) => {
//   const dancers = document.getElementsByClassName('image-holder');

//   Array.from(dancers).forEach((d) => {
//     d.classList.add('blur');
//     let index = String(data.key % 28);
//     if (index.length === 1) index = `0${index}`;
//     d.src = `./data/paulrudd/frame_${index}_delay-0.06s.gif`;
//   });
// });

// publishSubscribe.on('midikeyup', (data) => {
//   console.log('daddy');
//   if (Object.keys(registeringQueue.buffer).length === 0) {
//     const body = document.getElementsByTagName('body')[0];
//     const dancers = document.getElementsByClassName('image-holder');
//     body.classList.remove('body-rumble');
//     Array.from(dancers).forEach((d) => {
//       d.classList.remove('blur');
//     });
//   }
// });

publishSubscribe.on('midikeydown', (data) => {
  const index = (data.key - 28) % gifControls.length;
  let gifControl = gifControls[index];

  if (clearGIFTimers[index]) {
    clearTimeout(clearGIFTimers[index].destroyTimer);
    clearInterval(clearGIFTimers[index].opacityInterval);
    gifControl.element.style.opacity = 1;
  } else {
    clearGIFTimers[index] = {};
    gifControl.element.style.opacity = 1;
  }

  gifControl.element.src = gifControl.img + `?a=${Math.random()}`;
  gifControl.element.style.display = 'block';
  gifControl.element.style.zIndex = 10;

  clearGIFTimers[index].opacityInterval = setInterval(() => {
    let currentOpacity = Number(gifControl.element.style.opacity);
    currentOpacity -= 0.1;
    gifControl.element.style.opacity = String(currentOpacity);
    gifControl.element.style.zIndex = 1;
  }, gifControl.duration / 10);

  clearGIFTimers[index].destroyTimer = setTimeout(() => {
    gifControl.element.style.display = 'none';
    clearInterval(clearGIFTimers[index].opacityInterval);
  }, gifControl.duration);
});

function moveEmoji(emoji, location, velocity) {
  emoji.style.top = `${location[0]}px`;
  emoji.style.left = `${location[1]}px`;
  location[0] += velocity[0];
  location[1] += velocity[1];
}

function getIntegerInRange(min, max, index = false) {
  const range = [];
  let value = min;
  while (value <= max) {
    range.push(value);
    value++;
  }

  const set = index || Math.floor(Math.random() * (range.length - 1));

  return range[set];
}

setTimeout(async () => {
  playerNote(Player);
}, settings.oneBPM);

// (() => {
//   debug.initView({ element: document.getElementById('debug') });
//   publishSubscribe.on('midikeydown', (data) => {
//     debug.debugMessage(
//       'normal',
//       `key ${data.key} down with velocity ${data.velocity}`
//     );
//   });
//   publishSubscribe.on('midikeyup', (data) => {
//     debug.debugMessage('normal', `key ${data.key} up`);
//   });
// })();
