importScripts('/speak.js/speakGenerator.js');

onmessage = function (event) {
  console.log(event);
  postMessage(generateSpeech(event.data.text, event.data.args));
};
