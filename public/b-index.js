import Bapster from './bapster/bapster.js';
import Gifster from './gifster/index.js';
import { on } from './midibuddy/publishSubscribe.js';
import { getNoteForNonMIDIInput } from './midibuddy/midiToNoteName.js';

const gifUrls = [
  'http://localhost:3003/assets/1435457ipDcbK4i.gif',
  'http://localhost:3003/assets/spring.gif',
  'http://localhost:3003/assets/square.gif',
  'http://localhost:3003/assets/FEwtiPU7lBM3.gif',
];

const parent = document.getElementById('parent');
const gifsterContainer = document.getElementById('gifster-container');

const gifControls = gifUrls.map((path) => {
  return new Gifster(gifsterContainer, path);
});

let clearGIFTimers = {};

const bapsterPlayer = new Bapster(4, 6, parent);

const polySynth = new Tone.PolySynth(Tone.Synth).toDestination();

on('bapster-press', (data) => {
  console.log(data.id);
  console.log(getNoteForNonMIDIInput(data.id));
  polySynth.triggerAttackRelease(
    getNoteForNonMIDIInput(data.id),
    '4n',
    Tone.now()
  );
});

on('bapster-press', (data) => {
  const id = data.id;
  const index = data.id % gifControls.length;
  let gifControl = gifControls[index];

  if (clearGIFTimers[index]) {
    clearTimeout(clearGIFTimers[index]);
  }

  gifControl.element.src = gifControl.img + `?a=${Math.random()}`;
  gifControl.element.style.display = 'block';

  clearGIFTimers[index] = setTimeout(() => {
    gifControl.element.style.display = 'none';
  }, gifControl.duration);
});
