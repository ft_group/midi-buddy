import {
  StateMap,
  StateEvent,
  StateNode,
  Action,
} from './midibuddy/stateManager.js';
import EmoticonAnimator from './midibuddy/emoticonAnimator.js';
import { on, emit } from './midibuddy/publishSubscribe.js';
import { randomBetween, matchCaseInsensitive } from './midibuddy/utility.js';


const tremolo = new Tone.Tremolo(9, 0.75).toDestination().start();

const polySynth = new Tone.PolySynth().toDestination(); //.connect(tremolo).toDestination();
polySynth.options.oscillator.type = 'sine';
polySynth.volume.value = -20;

const vol = new Tone.Volume(-20).toDestination();

// use this to pan the two oscillators hard left/right
const merge = new Tone.Merge().connect(vol);

// two oscillators panned hard left / hard right
const rightOsc = new Tone.Oscillator({
  type: 'sawtooth',
  volume: -20,
}).connect(merge, 0, 0);

const leftOsc = new Tone.Oscillator({
  type: 'square',
  volume: -20,
}).connect(merge, 0, 1);

// create an oscillation that goes from 0 to 1200
// connection it to the detune of the two oscillators
const detuneLFO = new Tone.LFO({
  type: 'square',
  min: 0,
  max: 1200,
})
  .fan(rightOsc.detune, leftOsc.detune)
  .start();

// the frequency signal
const frequency = new Tone.Signal(0.1);

// the move the 0 to 1 value into frequency range
const scale = new Tone.ScaleExp(110, 440);

// multiply the frequency by 2.5 to get a 10th above
const mult = new Tone.Multiply(2.5);

// chain the components together
frequency.chain(scale, mult);
scale.connect(rightOsc.frequency);
mult.connect(leftOsc.frequency);

// multiply the frequency by 2 to get the octave above
const detuneScale = new Tone.Scale(14, 4);
frequency.chain(detuneScale, detuneLFO.frequency);

const pluck = new Tone.PluckSynth().toDestination();

const output = document.getElementById('output');

const animationClasses = [
  'loader',
  'hover',
  'left',
  'right',
  'embarrassed',
  'pleasure',
  'pleasure-plus',
  'pleasure-plus-plus',
  'pleasure-plus-plus-plus',
  'death',
  'check',
  'nuh-uh',
  'nod',
  'pointing',
  'bump',
  'blush',
];

on('statechange', (data) => {
  if (matchCaseInsensitive(data, 'blink')) {
    pluck.triggerAttack('C6');
  } else if (
    matchCaseInsensitive(data, 'right') ||
    matchCaseInsensitive(data, 'left')
  ) {
    //    if I can figure out an air rushing type noise
    //    that could be cool
  } else if (data === 'pleasure') {
    rightOsc.start();
    leftOsc.start();
    vol.set({ volume: -15 });
    frequency.rampTo(parseFloat(0.1), 0.1);
  } else if (data === 'pleasure+') {
    vol.set({ volume: -10 });
    frequency.rampTo(parseFloat(0.2), 0.3);
  } else if (data === 'pleasure++') {
    vol.set({ volume: -10 });
    frequency.rampTo(parseFloat(0.4), 0.3);
  } else if (data === 'pleasure+++') {
    vol.set({ volume: -10 });
    frequency.rampTo(parseFloat(0.8), 0.3);
  } else if (data === 'death') {
    frequency.rampTo(parseFloat(0.1), 0.3);
    setTimeout(() => {
      rightOsc.stop();
      leftOsc.stop();
    }, 300);
  } else if (data === 'lustyBump') {
    polySynth.triggerAttack(['C6', 'F6'], 1);
  } else {
    polySynth.triggerRelease(['C6', 'F6']);
    frequency.rampTo(parseFloat(0.1), 0.3);
    setTimeout(() => {
      rightOsc.stop();
      leftOsc.stop();
    }, 300);
  }
});

on('statechange', (data) => {
  console.log(data);
  console.log(matchCaseInsensitive(data, 'embarrassed'));
  removeClasses(animationClasses, output);

  if (matchCaseInsensitive(data, 'embarrassed')) {
    output.classList.add('embarrassed');
    console.log(output.classList);
  }

  if (matchCaseInsensitive(data, 'blush')) {
    output.classList.add('blush');
  }

  if (matchCaseInsensitive(data, 'left')) {
    output.classList.add('left');
    output.classList.add('hover');
    if (matchCaseInsensitive(data, 'nod')) {
      output.classList.add('nod');
    }
  } else if (matchCaseInsensitive(data, 'right')) {
    output.classList.add('right');
    output.classList.add('hover');
    if (matchCaseInsensitive(data, 'nod')) {
      output.classList.add('nod');
    }
  } else if (data === 'pleasure') {
    output.classList.add('pleasure');
  } else if (data === 'pleasure+') {
    output.classList.add('pleasure-plus');
  } else if (data === 'pleasure++') {
    output.classList.add('pleasure-plus-plus');
  } else if (data === 'pleasure+++') {
    output.classList.add('pleasure-plus-plus-plus');
  } else if (data === 'death') {
    output.classList.add('death');
  } else if (data === 'check') {
    output.classList.add('check');
  } else if (data === 'nuh-uh') {
    output.classList.add('nuh-uh');
  } else if (data === 'nod') {
    output.classList.add('nod');
  } else if (data === 'pointing') {
    output.classList.add('pointing');
  } else if (data === 'default') {
    output.classList.add('loader');
  } else if (data === 'lustyBump') {
    output.classList.add('bump');
  } else {
    output.classList.add('hover');
  }
});

let dirtyTimeout;
const dirtyWords = [
  {
    text: 'Yes daddy',
    wait: 1000,
  },
  {
    text: 'oo, baby',
    wait: 1000,
  },
  {
    text: 'fill all my ports',
    wait: 1000,
  },
  {
    text: "you're so deep in my directories",
    wait: 1500,
  },
];

let dirtyWordsStore = JSON.parse(JSON.stringify(dirtyWords));

function saySomethingDirty() {
  if (dirtyWordsStore.length === 0)
    dirtyWordsStore = JSON.parse(JSON.stringify(dirtyWords));
  const index = Math.floor(Math.random() * (dirtyWordsStore.length - 1));
  console.log(index, dirtyWordsStore, dirtyWordsStore.length - 1);
  const utterance = dirtyWordsStore[index];
  speak(utterance.text);

  dirtyWordsStore.splice(index, 1);

  dirtyTimeout = setTimeout(() => {
    saySomethingDirty();
  }, utterance.wait);
}

on('statechange', (data) => {
  if (data === 'pleasure+++') {
    saySomethingDirty();
    fetch("http://localhost:3003/vibrate/on")
  } else if (data === 'lustyBump') {
  } else {
    clearTimeout(dirtyTimeout);
    fetch('http://localhost:3003/vibrate/off')
  }
});

function removeClasses(classes, el) {
  classes.forEach((c) => el.classList.remove(c));
}

let TANDY;

fetch('./midibuddy/TANDY.json')
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    const states = Object.keys(data);
    const expressions = {};
    const links = {};

    states.forEach((key) => {
      links[key] = data[key].links;
      expressions[key] = data[key].animation;
    });

    TANDY = new EmoticonAnimator({
      element: output,
      expressions: expressions,
      start: true,
    });

    states.forEach((state) => {
      const stateLinks = links[state];
      stateLinks.forEach((link) => {
        mainState.addLink(state, link[1], link[0]);
      });
    });

    on('statechange', (stateData) => {
      let node = data[stateData];

      if (!node) node = { settings: {} };

      TANDY.setExpression(stateData, node.settings);
    });

    blinker(mainState);

    emit('statechange', mainState.getCurrentNode());
  });

function blinker(state) {
  const blinkWait = randomBetween(2, 10);

  setTimeout(() => {
    const result = state.triggerStateEvent(
      new StateEvent({ name: 'blink', data: {} })
    );
    if (result) emit('statechange', result);
    blinker(state);
    setTimeout(() => {
      const result = state.triggerStateEvent(
        new StateEvent({ name: 'unblink', data: {} })
      );
      if (result) emit('statechange', result);
    }, 125);
  }, blinkWait * 1000);
}

const mainState = new StateMap();

let pressed = {};

document.addEventListener('keydown', (ev) => handleKeyEvent('keydown', ev));

document.addEventListener('keyup', (ev) => handleKeyEvent('keyup', ev));

function handleKeyEvent(event, data) {
  const key = data.key;

  if (event === 'keydown' && pressed[key]) return;

  pressed[key] = event === 'keydown';

  const eventName = `${event}-${key}`;

  const result = mainState.triggerStateEvent(
    new StateEvent({ name: eventName, data: {} })
  );
  if (result) emit('statechange', result);
}
