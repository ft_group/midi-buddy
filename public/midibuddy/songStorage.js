import { now, nearestNumber } from "./utility.js";

class SongStorage {
  song = [];
  startOffset = 0;

  startNote(key, start, velocity) {
    this.song.push({ key, start, velocity });
  }

  endNote(key, start, end) {
    const note = this.song.filter((n) => {
      return n.start === start && n.key === key;
    })[0];

    note.end = end;
  }

  noteNext(note) {
    const runEnd = now();
    if (note.end === undefined) note.end = runEnd;
    const noteBank = this.song.filter((n) => {
      return n.start > note.end;
    });

    if (noteBank.length === 0) return false;

    return noteBank[0];
  }

  notePrev(note) {
    const noteBank = this.song.filter((n) => {
      return n.end < note.start;
    });

    if (noteBank.length === 0) return false;

    return noteBank[noteBank.length - 1];
  }

  noteRangeExclusive(start, end) {
    const runEnd = now();
    if (end === undefined) end = runEnd;
    return this.song.filter((n) => {
      if (!n.end) n.end = runEnd;
      return n.start >= start && n.end <= end;
    });
  }

  noteRangeInclusive(start, end) {
    const runEnd = now();
    if (end === undefined) end = runEnd;
    return this.song.filter((n) => {
      if (!n.end) n.end = runEnd;
      return (
        (n.end >= start && n.end <= end) || (n.start >= start && n.start <= end)
      );
    });
  }

  noteActiveAt(time) {
    return this.song.filter((n) => {
      n.start <= time && n.end >= time;
    });
  }

  noteHighest(notes) {
    return notes.sort((a, b) => {
      if (a.key < b.key) return 1;
      if (a.key > b.key) return -1;
      return 0;
    })[0];
  }

  noteLowest(notes) {
    return notes.sort((a, b) => {
      if (a.key > b.key) return 1;
      if (a.key < b.key) return -1;
      return 0;
    })[0];
  }

  getSong() {
    return this.song;
  }

  getMelody(notes) {
    const melody = [];

    let note = notes[0];
    while (note) {
      let coincidingNotes = this.noteRangeInclusive(note.start, note.end);
      let highestNote = this.noteHighest(coincidingNotes);
      melody.push(highestNote);
      note = this.noteNext(highestNote);
    }

    const startTime = melody[0].start;

    melody.forEach((n) => {
      n.start -= startTime;
      n.end -= startTime;
    });

    return melody;
  }
}

export default SongStorage;
