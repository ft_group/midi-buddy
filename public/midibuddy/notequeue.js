import { emit } from "./publishSubscribe.js";

export default class NoteQueue {
  buffer;
  depth = 12;
  constructor({ id, buffer, depth }) {
    this.id = id;
    this.buffer = buffer;
    if (depth) this.depth = depth;
  }

  bufferType() {
    return Array.isArray(this.buffer) ? "Array" : "Object";
  }

  addToQueue = (...args) => {
    const funct = Array.isArray(this.buffer)
      ? this.#addToArrayQueue
      : this.#addToObjectQueue;

    funct.apply(this, args);
  };

  #addToArrayQueue = (data) => {
    this.buffer.unshift(data);
    if (this.buffer.length > this.depth) {
      this.buffer.pop();
    }
    emit("notequeued", { source: this.id, data: data });
  };

  #addToObjectQueue = (data, name) => {
    this.buffer[name] = data;
    data = Object.assign(data, { key: name });
    emit("notequeued", { source: this.id, data: data });
  };

  getItemAtIndex = (index) => {
    return this.buffer[index];
  };

  clearItemAtIndex = (index) => {
    const funct = Array.isArray(this.buffer)
      ? this.#clearArrayItemAtIndex
      : this.#clearObjectItemAtIndex;
    funct(index);
  };

  #clearArrayItemAtIndex = (index) => {
    this.buffer.splice(index, 1);
  };

  #clearObjectItemAtIndex = (index) => {
    delete this.buffer[index];
  };
}
