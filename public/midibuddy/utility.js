export const now = () => {
  return new Date().getTime();
};

export const oneBPM = (tempo) => {
  return 60000 / tempo;
};

export const durationMap = (tempo, split, bottomNumber) => {
  return split.map((s) => (bottomNumber / s) * oneBPM(tempo));
};

export const nearestNumber = (value, values, index = false) => {
  const check = values.map((v) => Math.abs(v - value));
  const valueIndex = check.indexOf(Math.min(...check));
  return index ? valueIndex : values[valueIndex];
};

export const normalizeVelocity = (velocity) => {
  const velocityMax = 127;
  return velocity / velocityMax;
};

export const randomBetween = (min, max) => {
  const result = Math.round(Math.random() * (max - min) + min);
  return result;
};

export const matchCaseInsensitive = (str, check) => {
  const re = new RegExp(`${check}*`, 'gi');
  return re.test(str);
};
