export const analyze = (notes) => {
  let baseIndex = 0;
  const sequences = [];
  while (baseIndex < notes.length) {
    const sequence = getSequence(notes, baseIndex);

    baseIndex += 2;
  }
};

const getSequence = (notes, baseIndex) => {
  const base = notes[baseIndex];
  const implication = notes[baseIndex + 1];
  const realization = notes[baseIndex + 2];

  return [base, implication, realization];
};

const analyzeSequence = (sequence) => {
  const [base, mid, tri] = sequence;
  const antecedent = {
    intervalSize: intervalSize(base, mid),
    vector: vector(base, mid),
  };
  const realization = {
    intervalSize: intervalSize(mid, tri),
    vector: vector(mid, tri),
  };
};

const calculateType = (antecedent, realization) => {
  const intervalValue = antecedent.intervalSize + realization.intervalSize;
  const vectorValue = '';
};

const intervalSize = (note1, note2) => {
  const interval = Math.abs(note1 - note2);
  //  2 is large, 1 is small
  return interval > 6 ? 2 : 1;
};

const vector = (note1, note2) => {
  const value = note1 - note2;
  if (value === 0) return 0;
  if (value < 0) return 1;
  if (value > 0) return 3;
};
