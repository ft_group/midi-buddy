import * as publishSubscribe from './publishSubscribe.js';

export const initializeDeviceList = (parent) => {
  navigator.requestMIDIAccess().then(function (access) {
    const devices = Array.from(access.inputs.values());
    setupSelection(devices, parent);
    access.onstatechange = function (e) {
      replaceElements(devices, parent, false);
    };
  });
};

function setupSelection(devices, parent) {
  replaceElements(devices, parent);
  parent.addEventListener('change', (e) => {
    const index = e.target.value;
    const device = devices[index];

    device.onmidimessage = (m) => {
      parent.remove();
      const [command, key, velocity] = m.data;
      //  keydown
      if (command === 144) {
        publishSubscribe.emit('midikeydown', { key, velocity });
      } //  keyup
      else if (command === 128) {
        publishSubscribe.emit('midikeyup', { key });
      }
    };
  });
}

function replaceElements(devices, parent, hasBlankElement = true) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
  const elements = devices.map((d, i) => {
    const el = createOption({
      text: `${d.name} (${d.manufacturer})`,
      value: i,
    });
    return el;
  });

  if (hasBlankElement) {
    const blankElement = createOption({
      text: '-- select option below --',
      settings: [
        { name: 'disabled', value: true },
        { name: 'selected', value: true },
      ],
    });

    parent.appendChild(blankElement);
  }

  elements.forEach((e) => parent.appendChild(e));
}

function createOption({ text, value, settings }) {
  const el = document.createElement('option');
  el.innerText = text;
  if (value != undefined) {
    el.value = value;
  }
  if (settings) {
    settings.forEach((s) => {
      el[s.name] = s.value;
    });
  }
  return el;
}
