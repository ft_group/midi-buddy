import * as utility from "./utility.js";

const settings = {
  tempo: 84,
  timeSignature: [4, 4],
  quantizations: [8, 4, 2, 1],
};

settings.durationMap = utility.durationMap(
  settings.tempo,
  settings.quantizations,
  settings.timeSignature[1]
);

settings.oneBPM = utility.oneBPM(settings.tempo);

export default settings;
