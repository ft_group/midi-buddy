// import { Tone } from "../tone/build/esm/core/Tone.js";

import settings from "./settings.js";

export default class PlayerPolySynth {
  synth = new Tone.PolySynth(Tone.Synth).toDestination();

  constructor() {
    this.synth.options.oscillator.type = "sawtooth";
    Tone.Transport.bpm.value = settings.tempo;
  }

  triggerAttackRelease = (note, duration, velocity) => {
    this.synth.triggerAttackRelease(note, duration, Tone.now(), velocity);
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, this.synth.toSeconds(duration) * 1000);
    });
  };
}
