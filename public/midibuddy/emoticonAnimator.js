import { randomBetween } from './utility.js';

export default class EmoticonAnimator {
  element;
  expressions;
  currentExpression = 'default';
  frameDelay = 100;
  currentFrame = 0;
  random = false;
  animationTimeout;

  constructor({ element, expressions, start = false }) {
    this.element = element;
    this.expressions = expressions;
    if (start) this.draw();
  }

  setExpression(name, settings = {}) {
    if (settings.frameDelay !== undefined) {
      this.frameDelay = settings.frameDelay;
    }

    if (settings.random) {
      this.random = true;
    } else {
      this.random = false;
    }

    if (!this.expressions[name]) {
      throw new Error(`${name} is not a registered expression`);
      return;
    }

    this.currentExpression = name;
    this.resetAnimation();
  }

  getExpression() {
    return this.expressions[this.currentExpression];
  }

  setCurrentFrame(id) {
    this.currentFrame = id;
  }

  draw() {
    const expression = this.getExpression();
    let frameSet;
    if (this.random) {
      frameSet = randomBetween(0, expression.length - 1);
    } else {
      frameSet = this.currentFrame;
    }

    this.element.innerHTML = expression[frameSet];
    this.currentFrame++;

    if (this.currentFrame === expression.length) this.currentFrame = 0;

    if (this.frameDelay !== undefined) {
      this.animationTimeout = setTimeout(() => {
        this.draw();
      }, this.frameDelay);
    }
  }

  resetAnimation() {
    clearTimeout(this.animationTimeout);
    this.setCurrentFrame(0);
    this.draw();
  }
}
