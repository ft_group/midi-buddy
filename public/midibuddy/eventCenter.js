const listeners = {};

export const initializeEvents = (events) => {
  events.forEach((e) => {
    createEvent(e);
  });
};

export const createEvent = (name) => {
  if (!listeners[name]) listeners[name] = [];
};

export const deleteEvent = (name) => {
  if (!listeners[name]) return;
  delete listeners[name];
};

export const registerListener = (name, listener) => {
  if (typeof name === Array) {
    name.forEach((n) => {
      registerListener(n, listener);
    });
    return;
  }
  const list = listeners[name];
  if (!list) {
    throw new Error(
      `event ${name} was not registered. Run 'createEvent' before trying to register listeners`
    );
  }
  list.push(listener);
};

export const dispatchEvent = (name, data) => {
  const newEvent = new CustomEvent(name, data);
  const listenerList = listeners[name];
  if (!listenerList)
    throw new Error(
      `event ${name} was never registered. Run 'createEvent' before trying to run the event`
    );

  listenerList.forEach((l) => {
    l.dispatchEvent(newEvent);
  });
};
