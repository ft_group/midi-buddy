let parent;
let errorMax = 3;

export const initView = ({ element, count }) => {
  parent = element;
  if (count) errorMax = count;
};

export const debugMessage = (level, message) => {
  const el = createErrorElement(level, message);
  parent.prepend(el);
  if (parent.children.length > errorMax) parent.lastChild.remove();
};

const createErrorElement = (level, message) => {
  const el = document.createElement("div");
  el.classList.add(`debug-${level}`);
  el.innerHTML = message;

  return el;
};
