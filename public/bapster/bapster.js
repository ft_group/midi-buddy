import { emit } from '../midibuddy/publishSubscribe.js';

export default class Bapster {
  parent;
  constructor(rowCount, columnCount, parent) {
    this.parent = parent;

    const wrapper = createWrapperElement();

    const rows = new Array(rowCount).fill(0).forEach((r, i) => {
      const row = createRowElement(rowCount);
      const columns = new Array(columnCount).fill(0).forEach((c, j) => {
        const id = i * columnCount + j;
        const column = createColumnElement(columnCount, id);
        row.appendChild(column);
      });

      wrapper.appendChild(row);
    });

    parent.appendChild(wrapper);
  }
}

function createWrapperElement() {
  const wrapper = document.createElement('div');
  wrapper.classList.add('bapter-table');
  setStyles(wrapper, {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh',
    width: '100vw',
  });

  return wrapper;
}

function createRowElement(rowCount) {
  const row = document.createElement('div');
  row.classList.add('bapster-row');
  setStyles(row, {
    display: 'flex',
    width: '100%',
    flex: Math.floor(12 / rowCount),
  });

  return row;
}

function createColumnElement(columnCount, id) {
  const column = document.createElement('div');
  column.classList.add('bapster-element');
  setStyles(column, {
    flex: Math.floor(12 / columnCount),
  });

  column.setAttribute('b-id', String(id));
  column.addEventListener('mousedown', emitBapsterPress);
  column.addEventListener('touchstart', emitBapsterPress);

  return column;
}

function setStyles(element, styles) {
  Object.keys(styles).forEach((key) => {
    element.style[key] = styles[key];
  });
}

function emitBapsterPress(ev) {
  const id = ev.target.getAttribute('b-id');
  emit('bapster-press', { id });
}
