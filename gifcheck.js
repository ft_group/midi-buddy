import fetch from 'node-fetch';
import { parseGIF, decompressFrames } from 'gifuct-js';

var promisedGif = fetch('http://localhost:3003/assets/FEwtiPU7lBM3.gif')
  .then((resp) => resp.arrayBuffer())
  .then((buff) => parseGIF(buff))
  .then((gif) => {
    console.log(
      decompressFrames(gif, true)
        .map((f) => f.delay)
        .reduce((acc, x) => acc + x, 0)
    );
  });
