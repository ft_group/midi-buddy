//import Express from 'express';
//import gifDuration from './gifDuration.js';
const Express = require('express');
//const gifDuration = require('./gifDuration.js')
const GPIO = require('onoff').Gpio;
const vibrator1 = new GPIO(14, 'out');
const open = require('open');

const app = Express();
const port = 3003;

app.use(Express.json());

app.use(Express.static('public'));

//app.get('/gif-length/:path', async (req, res) => {
//  const duration = await gifDuration(req.params.path);
//
//  res.json({ duration });
//});

app.get('/vibrate/:setting', (req, res) => {

  const setting = req.params.setting === "on" ? 1 : 0;
  vibrator1.writeSync(setting)

})

app.listen(port, async() => {
  console.log(`listening on port ${port}`);
  await open('http://localhost:3003/animation-buddy.html')
});
