import fetch from 'node-fetch';
import { parseGIF, decompressFrames } from 'gifuct-js';

export default function gifDuration(path) {
  return new Promise((resolve, reject) => {
    var promisedGif = fetch(path)
      .then((resp) => resp.arrayBuffer())
      .then((buff) => parseGIF(buff))
      .then((gif) => {
        resolve(
          decompressFrames(gif, true)
            .map((f) => f.delay)
            .reduce((acc, x) => acc + x, 0)
        );
      });
  });
}
